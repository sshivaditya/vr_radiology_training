using System.Collections;
using System.Collections.Generic;
using System.IO;


using UnityEngine;

public class PausePlane : MonoBehaviour
{
   
    public Texture2D texture;
    public Vector2 textureSize = new Vector2(2048, 2048);

    void Start()
    {
        var r = GetComponent<Renderer>();
        texture = new Texture2D((int)textureSize.x, (int)textureSize.y);
        r.material.mainTexture = texture;

    }

    public void edittex()
    {
        var r = GetComponent<Renderer>();
        r.material.mainTexture = texture;
    }


    public void Save()
    {
        StartCoroutine(CoSave());
    }
    private IEnumerator CoSave()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log(Application.dataPath + "/savedImage.png");


        var data = texture.EncodeToPNG();
        File.WriteAllBytes(Application.dataPath + "/savedImage.png", data);
    }


}


