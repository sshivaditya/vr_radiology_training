﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* ErrorReporter_OnErrorReporter_m46827958528A1CB246EBEB6BF8E87DDFFF4934F7_RuntimeMethod_var;



// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mA721C989A1880CAA758436DC872C5D700C68BD25 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m148DD1A73FF770FA05485907B3E07CC9EFA80EDC (void);
// 0x00000003 System.Void TensorFlowLite.ErrorReporter::ConfigureReporter(System.IntPtr)
extern void ErrorReporter_ConfigureReporter_m39219CD00C4E3FDA30FFDFEB3D791D5EB885B1AA (void);
// 0x00000004 System.Void TensorFlowLite.ErrorReporter::OnErrorReporter(System.IntPtr,System.String,System.IntPtr)
extern void ErrorReporter_OnErrorReporter_m46827958528A1CB246EBEB6BF8E87DDFFF4934F7 (void);
// 0x00000005 System.Void TensorFlowLite.ErrorReporter::TfLiteInterpreterOptionsSetErrorReporter(System.IntPtr,TensorFlowLite.ErrorReporter/ErrorReporterDelegate,System.IntPtr)
extern void ErrorReporter_TfLiteInterpreterOptionsSetErrorReporter_m6CB8D17218C50977E8323FB5B662E8225B16B4DB (void);
// 0x00000006 System.String TensorFlowLite.ErrorReporter::UnityTFLiteStringFormat(System.String,System.IntPtr)
extern void ErrorReporter_UnityTFLiteStringFormat_m65FEE7CBE4019F5825FA414B79252A26A62B9A0D (void);
// 0x00000007 System.Void TensorFlowLite.ErrorReporter/ErrorReporterDelegate::.ctor(System.Object,System.IntPtr)
extern void ErrorReporterDelegate__ctor_m7B2D0BD08C37E3E86AB0F0F08C545305FAC4B9F0 (void);
// 0x00000008 System.Void TensorFlowLite.ErrorReporter/ErrorReporterDelegate::Invoke(System.IntPtr,System.String,System.IntPtr)
extern void ErrorReporterDelegate_Invoke_m39BFBD2DF2823ED50017C69EF23ECAA952276128 (void);
// 0x00000009 System.IAsyncResult TensorFlowLite.ErrorReporter/ErrorReporterDelegate::BeginInvoke(System.IntPtr,System.String,System.IntPtr,System.AsyncCallback,System.Object)
extern void ErrorReporterDelegate_BeginInvoke_m29B3DA859060F5352C080ECF680A96AF3C293D47 (void);
// 0x0000000A System.Void TensorFlowLite.ErrorReporter/ErrorReporterDelegate::EndInvoke(System.IAsyncResult)
extern void ErrorReporterDelegate_EndInvoke_m084CC5994D686A52062376DC5AFBE8C8D82FCF45 (void);
// 0x0000000B System.Void TensorFlowLite.Interpreter::.ctor(System.Byte[],TensorFlowLite.InterpreterOptions)
extern void Interpreter__ctor_mF1E8928B8C931C4786E6D5302D200E7616B676D3 (void);
// 0x0000000C System.Void TensorFlowLite.Interpreter::Dispose()
extern void Interpreter_Dispose_mA3DB3B46FF7C3047FCFC47CC627D71488C8061B5 (void);
// 0x0000000D System.Void TensorFlowLite.Interpreter::Invoke()
extern void Interpreter_Invoke_m228B38FCA1CCEDE758CBA924CC325BC7C8732AA6 (void);
// 0x0000000E System.Void TensorFlowLite.Interpreter::SetInputTensorData(System.Int32,System.Array)
extern void Interpreter_SetInputTensorData_m8E2595ECA226081E831D54C19B3035E86315604A (void);
// 0x0000000F System.Void TensorFlowLite.Interpreter::ResizeInputTensor(System.Int32,System.Int32[])
extern void Interpreter_ResizeInputTensor_mEA17BDF5F7ADE68652E3606CF4B744683AEA668A (void);
// 0x00000010 System.Void TensorFlowLite.Interpreter::AllocateTensors()
extern void Interpreter_AllocateTensors_m87C3D29EA509021F9B6E30E3E717548BC0781DB3 (void);
// 0x00000011 System.Void TensorFlowLite.Interpreter::GetOutputTensorData(System.Int32,System.Array)
extern void Interpreter_GetOutputTensorData_mC553A53E5016BA9A81FD061A85C1557714C622E5 (void);
// 0x00000012 System.String TensorFlowLite.Interpreter::GetTensorName(System.IntPtr)
extern void Interpreter_GetTensorName_m789E4ACB99A50F75B84681F8CC500287003FCB3B (void);
// 0x00000013 TensorFlowLite.Interpreter/TensorInfo TensorFlowLite.Interpreter::GetTensorInfo(System.IntPtr)
extern void Interpreter_GetTensorInfo_mA5E98D07988B26FF9926FF3802099B51F6C00CC8 (void);
// 0x00000014 System.Void TensorFlowLite.Interpreter::ThrowIfError(TensorFlowLite.Interpreter/Status)
extern void Interpreter_ThrowIfError_mD9AC291466EA4FAA42C59F7AA36BAA9286792298 (void);
// 0x00000015 System.IntPtr TensorFlowLite.Interpreter::TfLiteModelCreate(System.IntPtr,System.Int32)
extern void Interpreter_TfLiteModelCreate_m89DA4A8D9E41006AC6460276528DE61E1CA594C9 (void);
// 0x00000016 System.Void TensorFlowLite.Interpreter::TfLiteModelDelete(System.IntPtr)
extern void Interpreter_TfLiteModelDelete_m600AE3A0AA1FB351BD00F07417086A15815C5A60 (void);
// 0x00000017 System.IntPtr TensorFlowLite.Interpreter::TfLiteInterpreterCreate(System.IntPtr,System.IntPtr)
extern void Interpreter_TfLiteInterpreterCreate_m59679647837EBEADA0D889BF124811535293D1A5 (void);
// 0x00000018 System.Void TensorFlowLite.Interpreter::TfLiteInterpreterDelete(System.IntPtr)
extern void Interpreter_TfLiteInterpreterDelete_mDBADEA03FF68E11454A0305C9C8F71744A5236F6 (void);
// 0x00000019 System.IntPtr TensorFlowLite.Interpreter::TfLiteInterpreterGetInputTensor(System.IntPtr,System.Int32)
extern void Interpreter_TfLiteInterpreterGetInputTensor_mCAADED55C4420901610DD5734D4AD3F5BA83DC0A (void);
// 0x0000001A TensorFlowLite.Interpreter/Status TensorFlowLite.Interpreter::TfLiteInterpreterResizeInputTensor(System.IntPtr,System.Int32,System.Int32[],System.Int32)
extern void Interpreter_TfLiteInterpreterResizeInputTensor_m307802B59A2200D95F48B17F40416578BF696968 (void);
// 0x0000001B TensorFlowLite.Interpreter/Status TensorFlowLite.Interpreter::TfLiteInterpreterAllocateTensors(System.IntPtr)
extern void Interpreter_TfLiteInterpreterAllocateTensors_m7BA0CBCECD65A1FC061ABD5B3D3DD1775499404A (void);
// 0x0000001C TensorFlowLite.Interpreter/Status TensorFlowLite.Interpreter::TfLiteInterpreterInvoke(System.IntPtr)
extern void Interpreter_TfLiteInterpreterInvoke_mFB177A3C0D7EDF73045D8E952E051435BEF24CBF (void);
// 0x0000001D System.IntPtr TensorFlowLite.Interpreter::TfLiteInterpreterGetOutputTensor(System.IntPtr,System.Int32)
extern void Interpreter_TfLiteInterpreterGetOutputTensor_mF0D6705F26BE2D49C4CDFA59E5038B4C98B0836E (void);
// 0x0000001E TensorFlowLite.Interpreter/DataType TensorFlowLite.Interpreter::TfLiteTensorType(System.IntPtr)
extern void Interpreter_TfLiteTensorType_m5C720268382B76E918FE028B140BA3D6C91DE0FB (void);
// 0x0000001F System.Int32 TensorFlowLite.Interpreter::TfLiteTensorNumDims(System.IntPtr)
extern void Interpreter_TfLiteTensorNumDims_m3B7B8BB15E5C8808CE4B430C571F8E975A4210B4 (void);
// 0x00000020 System.Int32 TensorFlowLite.Interpreter::TfLiteTensorDim(System.IntPtr,System.Int32)
extern void Interpreter_TfLiteTensorDim_m694EDEC3C38A07E30B52F99375F6388F44C278BE (void);
// 0x00000021 System.IntPtr TensorFlowLite.Interpreter::TfLiteTensorName(System.IntPtr)
extern void Interpreter_TfLiteTensorName_m45B4CF90530E0171F63BA303BB95444C18056218 (void);
// 0x00000022 TensorFlowLite.Interpreter/QuantizationParams TensorFlowLite.Interpreter::TfLiteTensorQuantizationParams(System.IntPtr)
extern void Interpreter_TfLiteTensorQuantizationParams_mDF84C6660781360E49AA58A532F74A6B5333393C (void);
// 0x00000023 TensorFlowLite.Interpreter/Status TensorFlowLite.Interpreter::TfLiteTensorCopyFromBuffer(System.IntPtr,System.IntPtr,System.Int32)
extern void Interpreter_TfLiteTensorCopyFromBuffer_m214FACD0A3EA3896904B8B27F5E4B1A8CE13499A (void);
// 0x00000024 TensorFlowLite.Interpreter/Status TensorFlowLite.Interpreter::TfLiteTensorCopyToBuffer(System.IntPtr,System.IntPtr,System.Int32)
extern void Interpreter_TfLiteTensorCopyToBuffer_m05FF7E7947B3D1936634A234254F2CE81F1551BF (void);
// 0x00000025 System.String TensorFlowLite.Interpreter/TensorInfo::get_name()
extern void TensorInfo_get_name_mCB0D9F656BA0DD728EC65210C758C2674E7E58A2 (void);
// 0x00000026 System.Void TensorFlowLite.Interpreter/TensorInfo::set_name(System.String)
extern void TensorInfo_set_name_mA543F4D4CD441D7DD4FFFECE67FDF64168EA6F59 (void);
// 0x00000027 TensorFlowLite.Interpreter/DataType TensorFlowLite.Interpreter/TensorInfo::get_type()
extern void TensorInfo_get_type_mD7B42EBA5FE664AB70FB3C4712C5B996ACB17CC9 (void);
// 0x00000028 System.Void TensorFlowLite.Interpreter/TensorInfo::set_type(TensorFlowLite.Interpreter/DataType)
extern void TensorInfo_set_type_m9F75DBE8AEE94CC82FA7E3CA5BEABE0DBA6E4D5D (void);
// 0x00000029 System.Int32[] TensorFlowLite.Interpreter/TensorInfo::get_shape()
extern void TensorInfo_get_shape_m1BA33C73F9831666859EF1E761FF018769ACAEC9 (void);
// 0x0000002A System.Void TensorFlowLite.Interpreter/TensorInfo::set_shape(System.Int32[])
extern void TensorInfo_set_shape_m2CAE37B2EEB359167C22B062BF5E719D2E20EE38 (void);
// 0x0000002B TensorFlowLite.Interpreter/QuantizationParams TensorFlowLite.Interpreter/TensorInfo::get_quantizationParams()
extern void TensorInfo_get_quantizationParams_mB9F9810C0BDA7D590CAD896DE0E74AF8EE2343A7 (void);
// 0x0000002C System.Void TensorFlowLite.Interpreter/TensorInfo::set_quantizationParams(TensorFlowLite.Interpreter/QuantizationParams)
extern void TensorInfo_set_quantizationParams_m4D0D1036001F49A0D6D9B3AD9201756D7CB2A740 (void);
// 0x0000002D System.String TensorFlowLite.Interpreter/TensorInfo::ToString()
extern void TensorInfo_ToString_m53C70E3F7F5A6D36408C1204DD0C934AD8D38C63 (void);
// 0x0000002E System.String TensorFlowLite.Interpreter/QuantizationParams::ToString()
extern void QuantizationParams_ToString_mC1AD85CE3CD10C3BB981A9A2F5ADEAFB7E525F92 (void);
// 0x0000002F System.Void TensorFlowLite.InterpreterExperimental::TfLiteInterpreterOptionsSetUseNNAPI(System.IntPtr,System.Boolean)
extern void InterpreterExperimental_TfLiteInterpreterOptionsSetUseNNAPI_mC6D089C3B4161E14DDDC158CD76A24757FA87ADF (void);
// 0x00000030 System.Void TensorFlowLite.InterpreterOptions::set_threads(System.Int32)
extern void InterpreterOptions_set_threads_m56CE436B165FDDB99C3BB1ED481D51A839A3CE67 (void);
// 0x00000031 System.Void TensorFlowLite.InterpreterOptions::set_useNNAPI(System.Boolean)
extern void InterpreterOptions_set_useNNAPI_mC1E581EF4C8C5E3C11D5765C18801D3E6A0A4EE0 (void);
// 0x00000032 System.Void TensorFlowLite.InterpreterOptions::.ctor()
extern void InterpreterOptions__ctor_mD75762A156970C68CF0CD31C42736CA2CF3B813E (void);
// 0x00000033 System.Void TensorFlowLite.InterpreterOptions::Dispose()
extern void InterpreterOptions_Dispose_m324112C3E24DC6FE78463DFFCEBD5EC38D022236 (void);
// 0x00000034 System.IntPtr TensorFlowLite.InterpreterOptions::TfLiteInterpreterOptionsCreate()
extern void InterpreterOptions_TfLiteInterpreterOptionsCreate_m1502A66E89FFE410ADBB49E103DA3765AC84157A (void);
// 0x00000035 System.Void TensorFlowLite.InterpreterOptions::TfLiteInterpreterOptionsDelete(System.IntPtr)
extern void InterpreterOptions_TfLiteInterpreterOptionsDelete_m1932391AAF4EA1B7F387CF0B61A34D0B860BABAD (void);
// 0x00000036 System.Void TensorFlowLite.InterpreterOptions::TfLiteInterpreterOptionsSetNumThreads(System.IntPtr,System.Int32)
extern void InterpreterOptions_TfLiteInterpreterOptionsSetNumThreads_mAFE3FC8181C0BFF2BED6A9CF06E64B100B3CD1C2 (void);
static Il2CppMethodPointer s_methodPointers[54] = 
{
	EmbeddedAttribute__ctor_mA721C989A1880CAA758436DC872C5D700C68BD25,
	IsReadOnlyAttribute__ctor_m148DD1A73FF770FA05485907B3E07CC9EFA80EDC,
	ErrorReporter_ConfigureReporter_m39219CD00C4E3FDA30FFDFEB3D791D5EB885B1AA,
	ErrorReporter_OnErrorReporter_m46827958528A1CB246EBEB6BF8E87DDFFF4934F7,
	ErrorReporter_TfLiteInterpreterOptionsSetErrorReporter_m6CB8D17218C50977E8323FB5B662E8225B16B4DB,
	ErrorReporter_UnityTFLiteStringFormat_m65FEE7CBE4019F5825FA414B79252A26A62B9A0D,
	ErrorReporterDelegate__ctor_m7B2D0BD08C37E3E86AB0F0F08C545305FAC4B9F0,
	ErrorReporterDelegate_Invoke_m39BFBD2DF2823ED50017C69EF23ECAA952276128,
	ErrorReporterDelegate_BeginInvoke_m29B3DA859060F5352C080ECF680A96AF3C293D47,
	ErrorReporterDelegate_EndInvoke_m084CC5994D686A52062376DC5AFBE8C8D82FCF45,
	Interpreter__ctor_mF1E8928B8C931C4786E6D5302D200E7616B676D3,
	Interpreter_Dispose_mA3DB3B46FF7C3047FCFC47CC627D71488C8061B5,
	Interpreter_Invoke_m228B38FCA1CCEDE758CBA924CC325BC7C8732AA6,
	Interpreter_SetInputTensorData_m8E2595ECA226081E831D54C19B3035E86315604A,
	Interpreter_ResizeInputTensor_mEA17BDF5F7ADE68652E3606CF4B744683AEA668A,
	Interpreter_AllocateTensors_m87C3D29EA509021F9B6E30E3E717548BC0781DB3,
	Interpreter_GetOutputTensorData_mC553A53E5016BA9A81FD061A85C1557714C622E5,
	Interpreter_GetTensorName_m789E4ACB99A50F75B84681F8CC500287003FCB3B,
	Interpreter_GetTensorInfo_mA5E98D07988B26FF9926FF3802099B51F6C00CC8,
	Interpreter_ThrowIfError_mD9AC291466EA4FAA42C59F7AA36BAA9286792298,
	Interpreter_TfLiteModelCreate_m89DA4A8D9E41006AC6460276528DE61E1CA594C9,
	Interpreter_TfLiteModelDelete_m600AE3A0AA1FB351BD00F07417086A15815C5A60,
	Interpreter_TfLiteInterpreterCreate_m59679647837EBEADA0D889BF124811535293D1A5,
	Interpreter_TfLiteInterpreterDelete_mDBADEA03FF68E11454A0305C9C8F71744A5236F6,
	Interpreter_TfLiteInterpreterGetInputTensor_mCAADED55C4420901610DD5734D4AD3F5BA83DC0A,
	Interpreter_TfLiteInterpreterResizeInputTensor_m307802B59A2200D95F48B17F40416578BF696968,
	Interpreter_TfLiteInterpreterAllocateTensors_m7BA0CBCECD65A1FC061ABD5B3D3DD1775499404A,
	Interpreter_TfLiteInterpreterInvoke_mFB177A3C0D7EDF73045D8E952E051435BEF24CBF,
	Interpreter_TfLiteInterpreterGetOutputTensor_mF0D6705F26BE2D49C4CDFA59E5038B4C98B0836E,
	Interpreter_TfLiteTensorType_m5C720268382B76E918FE028B140BA3D6C91DE0FB,
	Interpreter_TfLiteTensorNumDims_m3B7B8BB15E5C8808CE4B430C571F8E975A4210B4,
	Interpreter_TfLiteTensorDim_m694EDEC3C38A07E30B52F99375F6388F44C278BE,
	Interpreter_TfLiteTensorName_m45B4CF90530E0171F63BA303BB95444C18056218,
	Interpreter_TfLiteTensorQuantizationParams_mDF84C6660781360E49AA58A532F74A6B5333393C,
	Interpreter_TfLiteTensorCopyFromBuffer_m214FACD0A3EA3896904B8B27F5E4B1A8CE13499A,
	Interpreter_TfLiteTensorCopyToBuffer_m05FF7E7947B3D1936634A234254F2CE81F1551BF,
	TensorInfo_get_name_mCB0D9F656BA0DD728EC65210C758C2674E7E58A2,
	TensorInfo_set_name_mA543F4D4CD441D7DD4FFFECE67FDF64168EA6F59,
	TensorInfo_get_type_mD7B42EBA5FE664AB70FB3C4712C5B996ACB17CC9,
	TensorInfo_set_type_m9F75DBE8AEE94CC82FA7E3CA5BEABE0DBA6E4D5D,
	TensorInfo_get_shape_m1BA33C73F9831666859EF1E761FF018769ACAEC9,
	TensorInfo_set_shape_m2CAE37B2EEB359167C22B062BF5E719D2E20EE38,
	TensorInfo_get_quantizationParams_mB9F9810C0BDA7D590CAD896DE0E74AF8EE2343A7,
	TensorInfo_set_quantizationParams_m4D0D1036001F49A0D6D9B3AD9201756D7CB2A740,
	TensorInfo_ToString_m53C70E3F7F5A6D36408C1204DD0C934AD8D38C63,
	QuantizationParams_ToString_mC1AD85CE3CD10C3BB981A9A2F5ADEAFB7E525F92,
	InterpreterExperimental_TfLiteInterpreterOptionsSetUseNNAPI_mC6D089C3B4161E14DDDC158CD76A24757FA87ADF,
	InterpreterOptions_set_threads_m56CE436B165FDDB99C3BB1ED481D51A839A3CE67,
	InterpreterOptions_set_useNNAPI_mC1E581EF4C8C5E3C11D5765C18801D3E6A0A4EE0,
	InterpreterOptions__ctor_mD75762A156970C68CF0CD31C42736CA2CF3B813E,
	InterpreterOptions_Dispose_m324112C3E24DC6FE78463DFFCEBD5EC38D022236,
	InterpreterOptions_TfLiteInterpreterOptionsCreate_m1502A66E89FFE410ADBB49E103DA3765AC84157A,
	InterpreterOptions_TfLiteInterpreterOptionsDelete_m1932391AAF4EA1B7F387CF0B61A34D0B860BABAD,
	InterpreterOptions_TfLiteInterpreterOptionsSetNumThreads_mAFE3FC8181C0BFF2BED6A9CF06E64B100B3CD1C2,
};
extern void TensorInfo_get_name_mCB0D9F656BA0DD728EC65210C758C2674E7E58A2_AdjustorThunk (void);
extern void TensorInfo_set_name_mA543F4D4CD441D7DD4FFFECE67FDF64168EA6F59_AdjustorThunk (void);
extern void TensorInfo_get_type_mD7B42EBA5FE664AB70FB3C4712C5B996ACB17CC9_AdjustorThunk (void);
extern void TensorInfo_set_type_m9F75DBE8AEE94CC82FA7E3CA5BEABE0DBA6E4D5D_AdjustorThunk (void);
extern void TensorInfo_get_shape_m1BA33C73F9831666859EF1E761FF018769ACAEC9_AdjustorThunk (void);
extern void TensorInfo_set_shape_m2CAE37B2EEB359167C22B062BF5E719D2E20EE38_AdjustorThunk (void);
extern void TensorInfo_get_quantizationParams_mB9F9810C0BDA7D590CAD896DE0E74AF8EE2343A7_AdjustorThunk (void);
extern void TensorInfo_set_quantizationParams_m4D0D1036001F49A0D6D9B3AD9201756D7CB2A740_AdjustorThunk (void);
extern void TensorInfo_ToString_m53C70E3F7F5A6D36408C1204DD0C934AD8D38C63_AdjustorThunk (void);
extern void QuantizationParams_ToString_mC1AD85CE3CD10C3BB981A9A2F5ADEAFB7E525F92_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[10] = 
{
	{ 0x06000025, TensorInfo_get_name_mCB0D9F656BA0DD728EC65210C758C2674E7E58A2_AdjustorThunk },
	{ 0x06000026, TensorInfo_set_name_mA543F4D4CD441D7DD4FFFECE67FDF64168EA6F59_AdjustorThunk },
	{ 0x06000027, TensorInfo_get_type_mD7B42EBA5FE664AB70FB3C4712C5B996ACB17CC9_AdjustorThunk },
	{ 0x06000028, TensorInfo_set_type_m9F75DBE8AEE94CC82FA7E3CA5BEABE0DBA6E4D5D_AdjustorThunk },
	{ 0x06000029, TensorInfo_get_shape_m1BA33C73F9831666859EF1E761FF018769ACAEC9_AdjustorThunk },
	{ 0x0600002A, TensorInfo_set_shape_m2CAE37B2EEB359167C22B062BF5E719D2E20EE38_AdjustorThunk },
	{ 0x0600002B, TensorInfo_get_quantizationParams_mB9F9810C0BDA7D590CAD896DE0E74AF8EE2343A7_AdjustorThunk },
	{ 0x0600002C, TensorInfo_set_quantizationParams_m4D0D1036001F49A0D6D9B3AD9201756D7CB2A740_AdjustorThunk },
	{ 0x0600002D, TensorInfo_ToString_m53C70E3F7F5A6D36408C1204DD0C934AD8D38C63_AdjustorThunk },
	{ 0x0600002E, QuantizationParams_ToString_mC1AD85CE3CD10C3BB981A9A2F5ADEAFB7E525F92_AdjustorThunk },
};
static const int32_t s_InvokerIndices[54] = 
{
	2883,
	2883,
	4430,
	3740,
	3740,
	3915,
	1266,
	697,
	145,
	2285,
	1268,
	2883,
	2883,
	1148,
	1148,
	2883,
	1148,
	4330,
	4462,
	4428,
	3881,
	4430,
	3882,
	4430,
	3881,
	3277,
	4266,
	4266,
	3881,
	4266,
	4266,
	3856,
	4288,
	4461,
	3540,
	3540,
	2825,
	2285,
	2806,
	2268,
	2825,
	2285,
	2936,
	2379,
	2825,
	2825,
	4104,
	2268,
	2308,
	2883,
	2883,
	4509,
	4430,
	4101,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x06000004, 0,  (void**)&ErrorReporter_OnErrorReporter_m46827958528A1CB246EBEB6BF8E87DDFFF4934F7_RuntimeMethod_var, 0 },
};
extern const CustomAttributesCacheGenerator g_TensorFlowLite_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_TensorFlowLite_CodeGenModule;
const Il2CppCodeGenModule g_TensorFlowLite_CodeGenModule = 
{
	"TensorFlowLite.dll",
	54,
	s_methodPointers,
	10,
	s_adjustorThunks,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_TensorFlowLite_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
