﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m90D0B6DEB625101355554D49B2EE2FB67C875860 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_mF1843241F60B2240CFAE651F1FD8A7AE17E32ECD (void);
// 0x00000003 System.Void MobileNet::Start()
extern void MobileNet_Start_m547366EEA10B5C7ACFEAF9E2991FE7C174E3E566 (void);
// 0x00000004 System.Void MobileNet::OnDestroy()
extern void MobileNet_OnDestroy_m65017705469A407B36881558A88309D06CC59665 (void);
// 0x00000005 System.Void MobileNet::OnDrawTexture(UnityEngine.Texture2D)
extern void MobileNet_OnDrawTexture_m1817C1BD09EF964C4ADD7B36213C7C17FDBF9DC4 (void);
// 0x00000006 System.Void MobileNet::Invoke(UnityEngine.Texture2D)
extern void MobileNet_Invoke_m4776D665B6C581F84F4F5351BAD3CAB6D2D24D00 (void);
// 0x00000007 System.Void MobileNet::.ctor()
extern void MobileNet__ctor_m38985DA4AEDC8048A23AA48644699F08537C6874 (void);
// 0x00000008 System.Void PausePlane::Start()
extern void PausePlane_Start_m4750B93765515BB3BDBCA7E97B23E2C962D60420 (void);
// 0x00000009 System.Void PausePlane::edittex()
extern void PausePlane_edittex_m460A4EC22E9F71F5DC2902E7E5CBDD049189D57C (void);
// 0x0000000A System.Void PausePlane::Save()
extern void PausePlane_Save_m78D29F2B83DA2F87C56978855F48CF2E93CD46E3 (void);
// 0x0000000B System.Void PausePlane::.ctor()
extern void PausePlane__ctor_m8FD27CFA806B057E69C20089DEBE82017D122E37 (void);
// 0x0000000C System.Boolean Whiteboard::get_IsPressed()
extern void Whiteboard_get_IsPressed_mF1B4935099B51A3498FC0BFA9CC9764482FF3F3E (void);
// 0x0000000D System.Void Whiteboard::set_IsPressed(System.Boolean)
extern void Whiteboard_set_IsPressed_mEBADFBBF53A08D5734844400791A47E89057D9AA (void);
// 0x0000000E System.Void Whiteboard::Awake()
extern void Whiteboard_Awake_m0A77B51FB40937EE76EEB0A80617F9291717B604 (void);
// 0x0000000F System.Void Whiteboard::PlayPause()
extern void Whiteboard_PlayPause_mE1206758948B384ED7AC9E4CB268BFEBCCE95E9B (void);
// 0x00000010 System.Void Whiteboard::OnNewFrame(UnityEngine.Video.VideoPlayer,System.Int64)
extern void Whiteboard_OnNewFrame_m6579C262F534F5E65E2DB0A9FFC064E177A4E4E5 (void);
// 0x00000011 UnityEngine.Texture2D Whiteboard::Resize(UnityEngine.Texture2D,System.Int32,System.Int32)
extern void Whiteboard_Resize_m9A516A800615E0ABC3EAA11E9E1521A301E4D02E (void);
// 0x00000012 UnityEngine.Color32 Whiteboard::CalculateAverageColorFromTexture(UnityEngine.Texture2D)
extern void Whiteboard_CalculateAverageColorFromTexture_m846C45BC4A286A6A7BFDC01A8892FCE9EFFF0CEF (void);
// 0x00000013 System.Void Whiteboard::Start()
extern void Whiteboard_Start_m7C9478B33F6AC746ADD2578B340D804D7AB8F945 (void);
// 0x00000014 System.Void Whiteboard::Update()
extern void Whiteboard_Update_m0650CEA939508617841C2DE495E7CE3D0BCDD267 (void);
// 0x00000015 System.Void Whiteboard::OnDestroy()
extern void Whiteboard_OnDestroy_mA50D92F8FFBA4C296742DAD7D8A371FE6DCC7A22 (void);
// 0x00000016 System.Void Whiteboard::OnDrawTexture(UnityEngine.RenderTexture)
extern void Whiteboard_OnDrawTexture_mD6404E6952B91CF84907B420BAD88464EAFBF646 (void);
// 0x00000017 System.Void Whiteboard::Invoke(UnityEngine.RenderTexture)
extern void Whiteboard_Invoke_m02544E692AAC84BAEE5C3DC438B08180BCF6DF80 (void);
// 0x00000018 System.Void Whiteboard::ConveretTexture2dIntoRenderTexture(UnityEngine.RenderTexture&,UnityEngine.Texture2D&,System.Int32)
extern void Whiteboard_ConveretTexture2dIntoRenderTexture_m51C8A6999F265ED77FBE561613140A580EC9D8D4 (void);
// 0x00000019 System.Void Whiteboard::.ctor()
extern void Whiteboard__ctor_mCC19A00EDC2F7222C164E411940B7712D8570D4E (void);
// 0x0000001A System.Void Whiteboard::.cctor()
extern void Whiteboard__cctor_m26019FFFD8098BB84B98D211C991868E545282FB (void);
// 0x0000001B System.Void WhiteboardMarker::Start()
extern void WhiteboardMarker_Start_m1E6B5C733610DFB17D24937427B156A79C1FA4FC (void);
// 0x0000001C System.Void WhiteboardMarker::Update()
extern void WhiteboardMarker_Update_mC8886F52A0594BB3DA031210102E8D481A1FFC86 (void);
// 0x0000001D System.Void WhiteboardMarker::Draw()
extern void WhiteboardMarker_Draw_m0514C502A9E17A46FC593294E24A811E970E7739 (void);
// 0x0000001E System.Void WhiteboardMarker::.ctor()
extern void WhiteboardMarker__ctor_m74EB8B305273DE59D673C8B6FAE50BB44ADB03E5 (void);
static Il2CppMethodPointer s_methodPointers[30] = 
{
	EmbeddedAttribute__ctor_m90D0B6DEB625101355554D49B2EE2FB67C875860,
	IsReadOnlyAttribute__ctor_mF1843241F60B2240CFAE651F1FD8A7AE17E32ECD,
	MobileNet_Start_m547366EEA10B5C7ACFEAF9E2991FE7C174E3E566,
	MobileNet_OnDestroy_m65017705469A407B36881558A88309D06CC59665,
	MobileNet_OnDrawTexture_m1817C1BD09EF964C4ADD7B36213C7C17FDBF9DC4,
	MobileNet_Invoke_m4776D665B6C581F84F4F5351BAD3CAB6D2D24D00,
	MobileNet__ctor_m38985DA4AEDC8048A23AA48644699F08537C6874,
	PausePlane_Start_m4750B93765515BB3BDBCA7E97B23E2C962D60420,
	PausePlane_edittex_m460A4EC22E9F71F5DC2902E7E5CBDD049189D57C,
	PausePlane_Save_m78D29F2B83DA2F87C56978855F48CF2E93CD46E3,
	PausePlane__ctor_m8FD27CFA806B057E69C20089DEBE82017D122E37,
	Whiteboard_get_IsPressed_mF1B4935099B51A3498FC0BFA9CC9764482FF3F3E,
	Whiteboard_set_IsPressed_mEBADFBBF53A08D5734844400791A47E89057D9AA,
	Whiteboard_Awake_m0A77B51FB40937EE76EEB0A80617F9291717B604,
	Whiteboard_PlayPause_mE1206758948B384ED7AC9E4CB268BFEBCCE95E9B,
	Whiteboard_OnNewFrame_m6579C262F534F5E65E2DB0A9FFC064E177A4E4E5,
	Whiteboard_Resize_m9A516A800615E0ABC3EAA11E9E1521A301E4D02E,
	Whiteboard_CalculateAverageColorFromTexture_m846C45BC4A286A6A7BFDC01A8892FCE9EFFF0CEF,
	Whiteboard_Start_m7C9478B33F6AC746ADD2578B340D804D7AB8F945,
	Whiteboard_Update_m0650CEA939508617841C2DE495E7CE3D0BCDD267,
	Whiteboard_OnDestroy_mA50D92F8FFBA4C296742DAD7D8A371FE6DCC7A22,
	Whiteboard_OnDrawTexture_mD6404E6952B91CF84907B420BAD88464EAFBF646,
	Whiteboard_Invoke_m02544E692AAC84BAEE5C3DC438B08180BCF6DF80,
	Whiteboard_ConveretTexture2dIntoRenderTexture_m51C8A6999F265ED77FBE561613140A580EC9D8D4,
	Whiteboard__ctor_mCC19A00EDC2F7222C164E411940B7712D8570D4E,
	Whiteboard__cctor_m26019FFFD8098BB84B98D211C991868E545282FB,
	WhiteboardMarker_Start_m1E6B5C733610DFB17D24937427B156A79C1FA4FC,
	WhiteboardMarker_Update_mC8886F52A0594BB3DA031210102E8D481A1FFC86,
	WhiteboardMarker_Draw_m0514C502A9E17A46FC593294E24A811E970E7739,
	WhiteboardMarker__ctor_m74EB8B305273DE59D673C8B6FAE50BB44ADB03E5,
};
static const int32_t s_InvokerIndices[30] = 
{
	2883,
	2883,
	2883,
	2883,
	2285,
	2285,
	2883,
	2883,
	2883,
	2883,
	2883,
	2852,
	2308,
	2883,
	2883,
	1265,
	546,
	1438,
	2883,
	2883,
	2883,
	2285,
	2285,
	3694,
	2883,
	4542,
	2883,
	2883,
	2883,
	2883,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	30,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
