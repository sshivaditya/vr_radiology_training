﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m79242AE558A45E4D67D6D2AA012F72DED2764412 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_mA7B8A7966D0E713EE0D0E64522A03562E490CAC7 (void);
// 0x00000003 System.Void TensorFlowLite.DisposeUtil::TryDispose(UnityEngine.RenderTexture)
extern void DisposeUtil_TryDispose_m511F71982A2C954FA7EE96677B4EDE1CB2D5AD73 (void);
// 0x00000004 System.Void TensorFlowLite.DisposeUtil::TryDispose(UnityEngine.Material)
extern void DisposeUtil_TryDispose_m7E4DDDABAD6479DD7CFD027B35D310FF4BF7EC56 (void);
// 0x00000005 System.Void TensorFlowLite.FilePopupAttribute::.ctor(System.String)
extern void FilePopupAttribute__ctor_mAAA683B6E3209F696A0BDE1388B0247EDC13C8D0 (void);
// 0x00000006 System.Byte[] TensorFlowLite.FileUtil::LoadFile(System.String)
extern void FileUtil_LoadFile_mBFBD7514C7C08FB06B6C1479EE9536B709CDF26C (void);
// 0x00000007 System.Boolean TensorFlowLite.FileUtil::IsPathRooted(System.String)
extern void FileUtil_IsPathRooted_m567CB104152C317F689C8B096058C4DB20BF5477 (void);
// 0x00000008 UnityEngine.Material TensorFlowLite.TextureResizer::get_material()
extern void TextureResizer_get_material_m0B18F5F1E3C7D80EA992AF843FC64763F708415D (void);
// 0x00000009 System.Void TensorFlowLite.TextureResizer::set_UVRect(UnityEngine.Vector4)
extern void TextureResizer_set_UVRect_m21D8D230A9359CC20757EAB97FDC05BA45FC193A (void);
// 0x0000000A System.Void TensorFlowLite.TextureResizer::set_VertexTransfrom(UnityEngine.Matrix4x4)
extern void TextureResizer_set_VertexTransfrom_m1F627C43C7FB2EA2A7F209497C589A258BB2E874 (void);
// 0x0000000B System.Void TensorFlowLite.TextureResizer::.ctor()
extern void TextureResizer__ctor_m6721BA5062E7500D5262399E220ECEAF4CA1A054 (void);
// 0x0000000C System.Void TensorFlowLite.TextureResizer::Dispose()
extern void TextureResizer_Dispose_mA290FC8051124CE3BB5F16C5B8DCD1D12596E286 (void);
// 0x0000000D UnityEngine.RenderTexture TensorFlowLite.TextureResizer::Resize(UnityEngine.Texture,System.Int32,System.Int32,System.Boolean,UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void TextureResizer_Resize_mA28F7C774F4F4437234A51906E49E9AFABDA274E (void);
// 0x0000000E UnityEngine.RenderTexture TensorFlowLite.TextureResizer::ApplyResize(UnityEngine.Texture,System.Int32,System.Int32,System.Boolean)
extern void TextureResizer_ApplyResize_m9253638EBFF235292C03083B41568D752FDC315E (void);
// 0x0000000F UnityEngine.Vector4 TensorFlowLite.TextureResizer::GetTextureST(System.Single,System.Single,TensorFlowLite.AspectMode)
extern void TextureResizer_GetTextureST_mAE670BDA6C1CEA54BD9887FCA1528CB668BB85DE (void);
// 0x00000010 UnityEngine.Matrix4x4 TensorFlowLite.TextureResizer::GetVertTransform(System.Single,System.Boolean,System.Boolean)
extern void TextureResizer_GetVertTransform_mE13D33CB6EA4D0F19499C928708D0DEC4E80DD18 (void);
// 0x00000011 System.Void TensorFlowLite.TextureResizer::.cctor()
extern void TextureResizer__cctor_mA64E1A4C56B19CC8D70E4AAD7ADA1C5B389690CB (void);
// 0x00000012 System.Void TensorFlowLite.WebCamInput::Start()
extern void WebCamInput_Start_mC27AD667C6EA67A508FB1F4286663C0201635CCF (void);
// 0x00000013 System.Void TensorFlowLite.WebCamInput::OnDestroy()
extern void WebCamInput_OnDestroy_mF93CEECF004ED740B8D04DB183A9762EDD1C6678 (void);
// 0x00000014 System.Void TensorFlowLite.WebCamInput::Update()
extern void WebCamInput_Update_m537AF99C5FCB462BFA38DE4C18A3EDC865A681D5 (void);
// 0x00000015 System.Void TensorFlowLite.WebCamInput::ToggleCamera()
extern void WebCamInput_ToggleCamera_m29F960FED083A3DBA5FB22C7071527AE50919A02 (void);
// 0x00000016 System.Void TensorFlowLite.WebCamInput::StartCamera(UnityEngine.WebCamDevice)
extern void WebCamInput_StartCamera_m7E0B2CB2A9D4A68CA44B4238CEBDE2020BDAAD65 (void);
// 0x00000017 System.Void TensorFlowLite.WebCamInput::StopCamera()
extern void WebCamInput_StopCamera_m42717B91C108A06D2015D514DC45AAB6BC0EDD3C (void);
// 0x00000018 UnityEngine.RenderTexture TensorFlowLite.WebCamInput::NormalizeWebcam(UnityEngine.WebCamTexture,System.Int32,System.Int32,System.Boolean)
extern void WebCamInput_NormalizeWebcam_m4895B5D2B58B6549F0060AE2495EF9FAC7C4FA37 (void);
// 0x00000019 System.Boolean TensorFlowLite.WebCamInput::IsPortrait(UnityEngine.WebCamTexture)
extern void WebCamInput_IsPortrait_m4C887381CEFBA19F31F04E86194A769FA51EBE76 (void);
// 0x0000001A System.Void TensorFlowLite.WebCamInput::.ctor()
extern void WebCamInput__ctor_m43B8E3A89BB077CE77F7CBC2F4ACFE29B42E1E81 (void);
// 0x0000001B System.Void TensorFlowLite.WebCamInput/TextureUpdateEvent::.ctor()
extern void TextureUpdateEvent__ctor_m82C498637227A548FBBEC520ABC7265742FE7085 (void);
// 0x0000001C System.Void TensorFlowLite.WebCamName::.ctor()
extern void WebCamName__ctor_m57DFAF16C44F58AC78F61ACB7B959B31DCB14069 (void);
// 0x0000001D System.String TensorFlowLite.WebCamUtil::FindName(TensorFlowLite.WebCamUtil/PreferSpec)
extern void WebCamUtil_FindName_mAA754FD0B2643CCD6857359BE566C0C9F0C54AED (void);
// 0x0000001E System.String TensorFlowLite.WebCamUtil::FindName(UnityEngine.WebCamKind,System.Boolean)
extern void WebCamUtil_FindName_m60A077352689F14A0D8D4BA7952447DE25913E46 (void);
// 0x0000001F System.Void TensorFlowLite.WebCamUtil/PreferSpec::.ctor(UnityEngine.WebCamKind,System.Boolean)
extern void PreferSpec__ctor_mF3A62067091086890C9F1CF7FEF24CC16A87E4DA (void);
// 0x00000020 System.Int32 TensorFlowLite.WebCamUtil/PreferSpec::GetScore(UnityEngine.WebCamDevice&)
extern void PreferSpec_GetScore_m40DC4E1DC64F9664D81395A25647DEC82B8C1A3A (void);
// 0x00000021 System.Void TensorFlowLite.WebCamUtil/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m9550372A3DDFD90E3D7CC0C8BD9B25B135253D08 (void);
// 0x00000022 System.Int32 TensorFlowLite.WebCamUtil/<>c__DisplayClass1_0::<FindName>b__0(UnityEngine.WebCamDevice)
extern void U3CU3Ec__DisplayClass1_0_U3CFindNameU3Eb__0_mAEBC9ABF478C8F145C2D47E8E56F0D9D06A6617F (void);
static Il2CppMethodPointer s_methodPointers[34] = 
{
	EmbeddedAttribute__ctor_m79242AE558A45E4D67D6D2AA012F72DED2764412,
	IsReadOnlyAttribute__ctor_mA7B8A7966D0E713EE0D0E64522A03562E490CAC7,
	DisposeUtil_TryDispose_m511F71982A2C954FA7EE96677B4EDE1CB2D5AD73,
	DisposeUtil_TryDispose_m7E4DDDABAD6479DD7CFD027B35D310FF4BF7EC56,
	FilePopupAttribute__ctor_mAAA683B6E3209F696A0BDE1388B0247EDC13C8D0,
	FileUtil_LoadFile_mBFBD7514C7C08FB06B6C1479EE9536B709CDF26C,
	FileUtil_IsPathRooted_m567CB104152C317F689C8B096058C4DB20BF5477,
	TextureResizer_get_material_m0B18F5F1E3C7D80EA992AF843FC64763F708415D,
	TextureResizer_set_UVRect_m21D8D230A9359CC20757EAB97FDC05BA45FC193A,
	TextureResizer_set_VertexTransfrom_m1F627C43C7FB2EA2A7F209497C589A258BB2E874,
	TextureResizer__ctor_m6721BA5062E7500D5262399E220ECEAF4CA1A054,
	TextureResizer_Dispose_mA290FC8051124CE3BB5F16C5B8DCD1D12596E286,
	TextureResizer_Resize_mA28F7C774F4F4437234A51906E49E9AFABDA274E,
	TextureResizer_ApplyResize_m9253638EBFF235292C03083B41568D752FDC315E,
	TextureResizer_GetTextureST_mAE670BDA6C1CEA54BD9887FCA1528CB668BB85DE,
	TextureResizer_GetVertTransform_mE13D33CB6EA4D0F19499C928708D0DEC4E80DD18,
	TextureResizer__cctor_mA64E1A4C56B19CC8D70E4AAD7ADA1C5B389690CB,
	WebCamInput_Start_mC27AD667C6EA67A508FB1F4286663C0201635CCF,
	WebCamInput_OnDestroy_mF93CEECF004ED740B8D04DB183A9762EDD1C6678,
	WebCamInput_Update_m537AF99C5FCB462BFA38DE4C18A3EDC865A681D5,
	WebCamInput_ToggleCamera_m29F960FED083A3DBA5FB22C7071527AE50919A02,
	WebCamInput_StartCamera_m7E0B2CB2A9D4A68CA44B4238CEBDE2020BDAAD65,
	WebCamInput_StopCamera_m42717B91C108A06D2015D514DC45AAB6BC0EDD3C,
	WebCamInput_NormalizeWebcam_m4895B5D2B58B6549F0060AE2495EF9FAC7C4FA37,
	WebCamInput_IsPortrait_m4C887381CEFBA19F31F04E86194A769FA51EBE76,
	WebCamInput__ctor_m43B8E3A89BB077CE77F7CBC2F4ACFE29B42E1E81,
	TextureUpdateEvent__ctor_m82C498637227A548FBBEC520ABC7265742FE7085,
	WebCamName__ctor_m57DFAF16C44F58AC78F61ACB7B959B31DCB14069,
	WebCamUtil_FindName_mAA754FD0B2643CCD6857359BE566C0C9F0C54AED,
	WebCamUtil_FindName_m60A077352689F14A0D8D4BA7952447DE25913E46,
	PreferSpec__ctor_mF3A62067091086890C9F1CF7FEF24CC16A87E4DA,
	PreferSpec_GetScore_m40DC4E1DC64F9664D81395A25647DEC82B8C1A3A,
	U3CU3Ec__DisplayClass1_0__ctor_m9550372A3DDFD90E3D7CC0C8BD9B25B135253D08,
	U3CU3Ec__DisplayClass1_0_U3CFindNameU3Eb__0_mAEBC9ABF478C8F145C2D47E8E56F0D9D06A6617F,
};
extern void PreferSpec__ctor_mF3A62067091086890C9F1CF7FEF24CC16A87E4DA_AdjustorThunk (void);
extern void PreferSpec_GetScore_m40DC4E1DC64F9664D81395A25647DEC82B8C1A3A_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x0600001F, PreferSpec__ctor_mF3A62067091086890C9F1CF7FEF24CC16A87E4DA_AdjustorThunk },
	{ 0x06000020, PreferSpec_GetScore_m40DC4E1DC64F9664D81395A25647DEC82B8C1A3A_AdjustorThunk },
};
static const int32_t s_InvokerIndices[34] = 
{
	2883,
	2883,
	4431,
	4431,
	2285,
	4332,
	4378,
	2825,
	2335,
	2279,
	2883,
	2883,
	80,
	322,
	3690,
	3577,
	4542,
	2883,
	2883,
	2883,
	2883,
	2338,
	2883,
	322,
	4378,
	2883,
	2883,
	2883,
	4348,
	3904,
	1162,
	1549,
	2883,
	1627,
};
extern const CustomAttributesCacheGenerator g_TensorFlowLite_Common_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_TensorFlowLite_Common_CodeGenModule;
const Il2CppCodeGenModule g_TensorFlowLite_Common_CodeGenModule = 
{
	"TensorFlowLite.Common.dll",
	34,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_TensorFlowLite_Common_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
