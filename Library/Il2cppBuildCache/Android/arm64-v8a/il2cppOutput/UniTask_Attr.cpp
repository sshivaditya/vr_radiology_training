﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncMethodBuilderAttribute
struct AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// System.String
struct String_t;
// System.Runtime.CompilerServices.TupleElementNamesAttribute
struct TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* AsyncUniTaskMethodBuilder_1_t4D557296E0CB9D6B7B3E3D92BB4F7A602C3281B1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* AsyncUniTaskMethodBuilder_t495936A951BCC81F447818FAEFB46EA4E1D243F6_0_0_0_var;

struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Runtime.CompilerServices.AsyncMethodBuilderAttribute
struct AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.AsyncMethodBuilderAttribute::<BuilderType>k__BackingField
	Type_t * ___U3CBuilderTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBuilderTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30, ___U3CBuilderTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CBuilderTypeU3Ek__BackingField_0() const { return ___U3CBuilderTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CBuilderTypeU3Ek__BackingField_0() { return &___U3CBuilderTypeU3Ek__BackingField_0; }
	inline void set_U3CBuilderTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CBuilderTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBuilderTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.TupleElementNamesAttribute
struct TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String[] System.Runtime.CompilerServices.TupleElementNamesAttribute::_transformNames
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____transformNames_0;

public:
	inline static int32_t get_offset_of__transformNames_0() { return static_cast<int32_t>(offsetof(TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD, ____transformNames_0)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__transformNames_0() const { return ____transformNames_0; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__transformNames_0() { return &____transformNames_0; }
	inline void set__transformNames_0(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____transformNames_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transformNames_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RuntimeInitializeLoadType
struct RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmbeddedAttribute__ctor_mC769ACF9C1572F6365A7C40953BEFA5E213051BD (EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_mCABDFC55D836A65D3CF7D62E0F6F31B323130E80 (IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF * __this, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncMethodBuilderAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncMethodBuilderAttribute__ctor_mE063A71B5304DD0DD014F095271112AC8CBB9CDB (AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30 * __this, Type_t * ___builderType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.TupleElementNamesAttribute::.ctor(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___transformNames0, const RuntimeMethod* method);
static void UniTask_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x54\x61\x73\x6B\x2E\x54\x65\x78\x74\x4D\x65\x73\x68\x50\x72\x6F"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[1];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x54\x61\x73\x6B\x2E\x41\x64\x64\x72\x65\x73\x73\x61\x62\x6C\x65\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x54\x61\x73\x6B\x2E\x4C\x69\x6E\x71"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[4];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[5];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[6];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x54\x61\x73\x6B\x2E\x44\x4F\x54\x77\x65\x65\x6E"), NULL);
	}
}
static void EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87 * tmp = (EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87 *)cache->attributes[1];
		EmbeddedAttribute__ctor_mC769ACF9C1572F6365A7C40953BEFA5E213051BD(tmp, NULL);
	}
}
static void IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87 * tmp = (EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87 *)cache->attributes[1];
		EmbeddedAttribute__ctor_mC769ACF9C1572F6365A7C40953BEFA5E213051BD(tmp, NULL);
	}
}
static void AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30_CustomAttributesCacheGenerator_U3CBuilderTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncUnit_tF4D610196BF6CB9CB5032138DD11D52A01D3C210_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF * tmp = (IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mCABDFC55D836A65D3CF7D62E0F6F31B323130E80(tmp, NULL);
	}
}
static void CancellationTokenExtensions_t23150039E4970811FFB82D4455E541A749E60158_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CancellationTokenExtensions_t23150039E4970811FFB82D4455E541A749E60158_CustomAttributesCacheGenerator_CancellationTokenExtensions_RegisterWithoutCaptureExecutionContext_mFA67F8204DAD04F3F4E4CC416B5E8055A343A08F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UniTaskStatusExtensions_t99C133AD36EB3F216D23147CD00206BD441F0E4C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UniTaskStatusExtensions_t99C133AD36EB3F216D23147CD00206BD441F0E4C_CustomAttributesCacheGenerator_UniTaskStatusExtensions_IsCompleted_m7A9E3A6930C2EB3ED293F11782F2E0A16FAF8E71(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void InjectPlayerLoopTimings_t87083E6D4AD4D5F1019758428CB6D0D5DAC46326_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void PlayerLoopHelper_t2E5EE4D6CAA10845C9E0146D0A57E739FFA078DC_CustomAttributesCacheGenerator_U3CIsEditorApplicationQuittingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerLoopHelper_t2E5EE4D6CAA10845C9E0146D0A57E739FFA078DC_CustomAttributesCacheGenerator_PlayerLoopHelper_get_IsEditorApplicationQuitting_m41F5E63B94BAA7BC025829359D13C0101BD74551(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerLoopHelper_t2E5EE4D6CAA10845C9E0146D0A57E739FFA078DC_CustomAttributesCacheGenerator_PlayerLoopHelper_set_IsEditorApplicationQuitting_m8FE072C71C6415A874DC4E074DFA91B8E81C1630(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerLoopHelper_t2E5EE4D6CAA10845C9E0146D0A57E739FFA078DC_CustomAttributesCacheGenerator_PlayerLoopHelper_Init_mD46E29166AB848484B0BB715017D5CC9144E51E1(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_t02079ABD7972EDCB91CC778941FA7C5C567CBE6D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t26A1FEAFC5C610E465529EBD3C8CA7B19FD80B87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UniTask_tB2308A79346AE4017FEA07795B6E0A00C0D30B0F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncUniTaskMethodBuilder_t495936A951BCC81F447818FAEFB46EA4E1D243F6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF * tmp = (IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mCABDFC55D836A65D3CF7D62E0F6F31B323130E80(tmp, NULL);
	}
	{
		AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30 * tmp = (AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30 *)cache->attributes[1];
		AsyncMethodBuilderAttribute__ctor_mE063A71B5304DD0DD014F095271112AC8CBB9CDB(tmp, il2cpp_codegen_type_get_object(AsyncUniTaskMethodBuilder_t495936A951BCC81F447818FAEFB46EA4E1D243F6_0_0_0_var), NULL);
	}
}
static void UniTask_tB2308A79346AE4017FEA07795B6E0A00C0D30B0F_CustomAttributesCacheGenerator_UniTask__ctor_mA1D2FAF1D02391065D770F56EE3CFD028157CACD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTask_tB2308A79346AE4017FEA07795B6E0A00C0D30B0F_CustomAttributesCacheGenerator_UniTask_get_Status_m5CCD7973CFD636CF45F67AF23CC34349ABBAD0AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTask_tB2308A79346AE4017FEA07795B6E0A00C0D30B0F_CustomAttributesCacheGenerator_UniTask_GetAwaiter_m1FE35EBA18DB40141C1025CECD4FC300E3A8C59C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF * tmp = (IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mCABDFC55D836A65D3CF7D62E0F6F31B323130E80(tmp, NULL);
	}
}
static void Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter__ctor_m5C4926AF1F0B75F44AC954B944D307066AA132CF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter__ctor_m5C4926AF1F0B75F44AC954B944D307066AA132CF____task0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF * tmp = (IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mCABDFC55D836A65D3CF7D62E0F6F31B323130E80(tmp, NULL);
	}
}
static void Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter_get_IsCompleted_mDCC71AAE7FA5EA2B303D3EC512177FD122C8DA49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter_GetResult_mFCA8BA81A9A7DA641DEFC7FC121FED87A3EC06CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter_UnsafeOnCompleted_m1428D2FC0145C8CABB14E4B9A15F38B6AE34F8E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter_SourceOnCompleted_m71BEAA335CDD0FBD24F14F9B970B8E1B69AE01B4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t5BBC3DF5BC2006E9541B4BE76108484D5D31D29F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AwaiterActions_t98CE1739780D10DE070EA22469BDD661F7721BDD_CustomAttributesCacheGenerator_AwaiterActions_Continuation_m6C950CC84C028955999620A11573A87154D4F2B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTask_1_t9FE8B3D4B9F6652AE80DA9C9E7F3F0DD133D1D94_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncUniTaskMethodBuilder_1_t4D557296E0CB9D6B7B3E3D92BB4F7A602C3281B1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF * tmp = (IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mCABDFC55D836A65D3CF7D62E0F6F31B323130E80(tmp, NULL);
	}
	{
		AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30 * tmp = (AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30 *)cache->attributes[1];
		AsyncMethodBuilderAttribute__ctor_mE063A71B5304DD0DD014F095271112AC8CBB9CDB(tmp, il2cpp_codegen_type_get_object(AsyncUniTaskMethodBuilder_1_t4D557296E0CB9D6B7B3E3D92BB4F7A602C3281B1_0_0_0_var), NULL);
	}
}
static void UniTask_1_t9FE8B3D4B9F6652AE80DA9C9E7F3F0DD133D1D94_CustomAttributesCacheGenerator_UniTask_1__ctor_mA66DD0CCDBB13655E9DABDFBA3CB51287728DCE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_Reset_m43C8782C2188C269E53275F197E11FAC7080AE17(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_TrySetResult_mED45D3F73A294D098843FD7DB5755A3BC313D9D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_TrySetCanceled_m46448E2FAA596ED6FC4842180BCE4BC8EBB934DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_GetStatus_mA6CA4535C4A74CED6D362E8E9C97C5D1B422349B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_UnsafeGetStatus_m9801AAB80713DBD2CF427D616881D2C515670438(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_GetResult_mBA13129431126C629F6605F87934079053DB52C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_OnCompleted_m041EDDC3143C0479E4DBC228F1C0C131A253ECED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_ValidateToken_m180C8A5395E191052AE7D41B5A8936104B4BE64E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A____Version_PropertyInfo(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_MarkHandled_m3EABFFA54BF7934490BCA86DA8F76AA023BE8144(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_get_Task_m71FDFD642F5DE334B395253D95E812AF5E191CD1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_TrySetResult_mD0DF7B8E60DD4FE6D4FC5EE0386CE075805E12DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_GetResult_m39A120F5B2805CF60DF0DDDF56AD13E6FE6BDED1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_GetStatus_mED83FEAAD02A8E3145F2DA6625390DDFFC374002(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_UnsafeGetStatus_m4C223CE67FF35004CE732D6ED295B814E3FD9E02(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_OnCompleted_mFF8DB2962201801A755B6B2EC59EFF6B3ADA4CC3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_TrySignalCompletion_m1DF031CC4D1BC7CC43F6CF57FB5056173615B064(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UniTaskExtensions_t3DEAB33C9A9C207E6B284BB9BE4A70F923EC5F36_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UniTaskExtensions_t3DEAB33C9A9C207E6B284BB9BE4A70F923EC5F36_CustomAttributesCacheGenerator_UniTaskExtensions_Forget_m03A03134BA00CD83A83902AEF9352C8C0AC595A5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec_t148EAD9051E0C458493DFB2B714617F946384F50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UniTaskScheduler_t9DBF8570AC748696C52D015E46344646DFA4B230_CustomAttributesCacheGenerator_UnobservedTaskException(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Callback_t6F406C14EF31534B69D26D751E58D5DB304518CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF * tmp = (IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mCABDFC55D836A65D3CF7D62E0F6F31B323130E80(tmp, NULL);
	}
}
static void AsyncAwakeTrigger_t37FF5D07CEA74AAB7789063114CE2322B56F5ACD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncDestroyTrigger_tEB2DAC8A657F8BACC8573C7D5E687AD85EB9AB87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void U3CU3Ec_t66FC60B3AC0C44F718BF10164DE5C53556657AD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncStartTrigger_tF8F038F148FADE1C91DC5BF70D33E2A7B145CE3B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_U3CCurrentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_U3CCysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_PrevU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_U3CCysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_NextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_set_Current_m09CA8F609DF7A066B02848DBD323C745B94508FF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_get_Prev_m921BEAE8DCA173B0DD2C65F5FEDDA6134711C459(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_set_Prev_m9346EF053F0907FEBF8A15F16598192C0BD212C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_get_Next_m608A053D4480BC7511E28B439894055610D88E51(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_set_Next_m57744F4DBDA686A7447D9BA2D71E18ECAB3A6FDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_U3CCysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_PrevU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_U3CCysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_NextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_AsyncTriggerHandler_1_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_get_Prev_m76750E5C66C73E628A1321ABB67B4D38D33DFDAA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_AsyncTriggerHandler_1_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_set_Prev_mC5052FF72CAFC430F7C07D91BDCC571370D65EAE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_AsyncTriggerHandler_1_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_get_Next_m71AD68950B676B1AE5A547DF64D38ABA85D32EFC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_AsyncTriggerHandler_1_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_set_Next_m95AAB1B41971906EE6D9F1430D9E0889375A62CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsyncFixedUpdateTrigger_tA4486802106D891AD37E9EBEB2BD954D4D84070F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncLateUpdateTrigger_t9A23744715BB464A18983EFDA8572E7E072F2168_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncAnimatorIKTrigger_t24AD6CA9764F3989F15216EE3B65C5D1A3FB578B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncAnimatorMoveTrigger_t109E79622C585911298836CD8C085959205051F2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncApplicationFocusTrigger_t83032ABFF9213CDDE183A185023D79A09B30EF0D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncApplicationPauseTrigger_tFB7D6AF53DF627337CE24A5567B1878AC69EC8D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncApplicationQuitTrigger_t8D8588A06A61153E49C94D0873DA7C845558AA53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncAudioFilterReadTrigger_t9A0B9AD7E4D6221B45BFAD2A835C3538AB6E3D17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x64\x61\x74\x61"));
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x63\x68\x61\x6E\x6E\x65\x6C\x73"));
		TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * tmp = (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD *)cache->attributes[0];
		TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E(tmp, _tmp_0, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncBecameInvisibleTrigger_t7B3C4DA88E3A4E5B1599B158394247EE9A1C7416_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncBecameVisibleTrigger_t5CEB9F94A7DFEF8778D195406445FF0E6CF78DE8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncBeforeTransformParentChangedTrigger_t13CEAA0C6AFB2DCC5EF53AA599E09E81AA01BF14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncOnCanvasGroupChangedTrigger_t66DE0EE2E0D5450540DE5BFC91CA0B2FEC6314C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncCollisionEnterTrigger_t254EEA82B58D99B798A0638BC10FE77309A7A256_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncCollisionEnter2DTrigger_t887A6B3188DC7A14A7CA5F6CF01E4D2635C674B7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncCollisionExitTrigger_t0D0210C093D3725FE73ED6771AA1550423364524_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncCollisionExit2DTrigger_t98CB78C1463FCD7F69FC1B84207FE0B864F0410A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncCollisionStayTrigger_tCCDE9A96609FB1A658F91D54A691CD6704CAA882_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncCollisionStay2DTrigger_tDB4703E2F5EDA265DB5449D88FD5BC6AEB998BF2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncControllerColliderHitTrigger_t5134DD9C49CE4D2F2A67383C1A69DAFAA9D11A92_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncDisableTrigger_t8CA9C5606CB0022AA26AB462199E651123653090_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncDrawGizmosTrigger_t4958A3DF58C076C55DDD945EC07A00A6CC3498B3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncDrawGizmosSelectedTrigger_t57AB1A58E21CC60B54866CEB2A9FE5971E0A3074_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncEnableTrigger_tB634428E53814A596931A6D574315FD716189C50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncGUITrigger_t36C147776091A27EF7527BC98560F8EBB9E808EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncJointBreakTrigger_t874B2B61D4D48487690385F4E198E60C886A500B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncJointBreak2DTrigger_t3E666FA2DE906DE89CC79B692521AA6EE711934E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncParticleCollisionTrigger_t9D4FA39E8CADD41645732D3A820AE435219E2F5D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncParticleSystemStoppedTrigger_t0F0A22FA9EC6235AC4627C672F53BCA6D12467FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncParticleTriggerTrigger_t2BCAAA60F894FB4AE1C5A26356B269D8587C226D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncParticleUpdateJobScheduledTrigger_tBA9445AF4934EF9F52D3AC6598CAF9B5DF514167_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncPostRenderTrigger_tACEB9E79F5A6EE5510E8F74109640633BC6ECDB0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncPreCullTrigger_tDF118EEBDADECD321DDB7AD31B8120BC6818F9F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncPreRenderTrigger_tD2CBFB8EAEEBCDB5838C71AC873689FFF961E285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncRectTransformDimensionsChangeTrigger_t0419A603C2E72B207F0506CBA2F2BC6FC03B2457_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncRectTransformRemovedTrigger_tCDC782753E53846A9A9E308DDDD0E20EDC93C318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncRenderImageTrigger_t47A7E1D01FD9DB027C65E2064D2DD6253BC4AB0E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x73\x6F\x75\x72\x63\x65"));
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E"));
		TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * tmp = (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD *)cache->attributes[1];
		TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E(tmp, _tmp_0, NULL);
	}
}
static void AsyncRenderObjectTrigger_tB922BB87D329F98226329AA3E491F809B6379660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncServerInitializedTrigger_t087A376938D0580C2647B3B9B55E927E49C9FEBB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTransformChildrenChangedTrigger_tCDE97CE1BF36149A3CB55E4A82D692758EDAD7D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTransformParentChangedTrigger_t0C0B10322954D9ED5C953A11A6786BBCB68F6396_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTriggerEnterTrigger_t2E9351C93BCA4677BA6D92D4C0CF8C22261BFC3B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTriggerEnter2DTrigger_t999BD5719FBABEA2EDB0FEA5FDC8336A7DA76917_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTriggerExitTrigger_tD48E2EE94818B5C05D4C9E8106BD2597A101D3CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTriggerExit2DTrigger_t95DFB35379000F9CF3E4EA0601CD08127EC5462C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTriggerStayTrigger_t48B35A72F3BA61F203488A57DB8F2A18200E12A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncTriggerStay2DTrigger_tF772045DFB3DE353BA8175280CA52283D1EDFA11_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncValidateTrigger_t97D6D0AF3C77858DDB0100B7E104A5555EF04C02_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncWillRenderObjectTrigger_t24F3165363B8FF9783570A334C70F09BAE7A1BA4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncResetTrigger_tA217B969CD85B67D425B6B6F45E983643D73070F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncUpdateTrigger_t69C1AADCF9304CA32D768FB317C26365335C856E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncBeginDragTrigger_t0E46010A9E12309809FC2A823118141A9A7A1423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncCancelTrigger_tEF554944E4D80D51FA48B49D8A9182C75EC96D9D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncDeselectTrigger_tD36DCFE7A61E16AD00D20122783619413A5B19A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncDragTrigger_t94BA4F7B71CB126E1C5B2936C576DC7EA831E7B4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncDropTrigger_t8C7A57AD20C3902E80FEF896EB61A045E30FEE8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncEndDragTrigger_tDDB3674FA927983E51CA4CC1A03F23AB6A5D1846_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncInitializePotentialDragTrigger_t219531E2B739E95B96CE192BA23FDFF80A62AC99_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncMoveTrigger_t4FC53A1969EB5386E2D0B9B1088E858DD6896659_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncPointerClickTrigger_t421FEC992B38EF3FC608AF062BF0D613EFB39D06_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncPointerDownTrigger_t650AD4A1A7FD2A646205EC691F70079632D993F9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncPointerEnterTrigger_tE2D5FF7D6D00BE9AA4B2ED0C6817678D8D157747_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncPointerExitTrigger_tA1C1FB5030F175CF21438BFF4DA3D549B31815B7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncPointerUpTrigger_tA88BA67B00881197DE089742729D35CDA1A2320C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncScrollTrigger_t575F13F3F86F4177127C331BE6E37B8A65F1DA21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncSelectTrigger_t2394AE85B5E1897148B2CAD3DB12DEF5A77CA9EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncSubmitTrigger_t6BEA5852DA89E0A1358C4DCD4074E1B782969123_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void AsyncUpdateSelectedTrigger_t0F1DF2441746047AA160610572BA5E06DBCB1CCF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ContinuationQueue_t1AC570586E5949315476FE4A64A3D024C45886C2_CustomAttributesCacheGenerator_ContinuationQueue_RunCore_mA8B47350621EF411C87B9BF4255A6B608713CE65(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayerLoopRunner_t3F49822FCB616AEED5CCB70D8AF755B67A5ECB9C_CustomAttributesCacheGenerator_PlayerLoopRunner_RunCore_m6C97C81CE05C65E05140C6F3FA67F4ACCB908BC1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t06DEAF86CBB5C462C627811F9852BDFF005A2073_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_UniTask_AttributeGenerators[];
const CustomAttributesCacheGenerator g_UniTask_AttributeGenerators[141] = 
{
	EmbeddedAttribute_t8B8F7A8D48C4B0F4580F9C16F5E4AC9F6B4A7A87_CustomAttributesCacheGenerator,
	IsReadOnlyAttribute_tD953E07F54300CACE0D53C59A9975F9EA87B73EF_CustomAttributesCacheGenerator,
	AsyncUnit_tF4D610196BF6CB9CB5032138DD11D52A01D3C210_CustomAttributesCacheGenerator,
	CancellationTokenExtensions_t23150039E4970811FFB82D4455E541A749E60158_CustomAttributesCacheGenerator,
	UniTaskStatusExtensions_t99C133AD36EB3F216D23147CD00206BD441F0E4C_CustomAttributesCacheGenerator,
	InjectPlayerLoopTimings_t87083E6D4AD4D5F1019758428CB6D0D5DAC46326_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_t02079ABD7972EDCB91CC778941FA7C5C567CBE6D_CustomAttributesCacheGenerator,
	U3CU3Ec_t26A1FEAFC5C610E465529EBD3C8CA7B19FD80B87_CustomAttributesCacheGenerator,
	UniTask_tB2308A79346AE4017FEA07795B6E0A00C0D30B0F_CustomAttributesCacheGenerator,
	Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator,
	U3CU3Ec_t5BBC3DF5BC2006E9541B4BE76108484D5D31D29F_CustomAttributesCacheGenerator,
	UniTask_1_t9FE8B3D4B9F6652AE80DA9C9E7F3F0DD133D1D94_CustomAttributesCacheGenerator,
	UniTaskExtensions_t3DEAB33C9A9C207E6B284BB9BE4A70F923EC5F36_CustomAttributesCacheGenerator,
	U3CU3Ec_t148EAD9051E0C458493DFB2B714617F946384F50_CustomAttributesCacheGenerator,
	Callback_t6F406C14EF31534B69D26D751E58D5DB304518CF_CustomAttributesCacheGenerator,
	AsyncAwakeTrigger_t37FF5D07CEA74AAB7789063114CE2322B56F5ACD_CustomAttributesCacheGenerator,
	AsyncDestroyTrigger_tEB2DAC8A657F8BACC8573C7D5E687AD85EB9AB87_CustomAttributesCacheGenerator,
	U3CU3Ec_t66FC60B3AC0C44F718BF10164DE5C53556657AD3_CustomAttributesCacheGenerator,
	AsyncStartTrigger_tF8F038F148FADE1C91DC5BF70D33E2A7B145CE3B_CustomAttributesCacheGenerator,
	AsyncFixedUpdateTrigger_tA4486802106D891AD37E9EBEB2BD954D4D84070F_CustomAttributesCacheGenerator,
	AsyncLateUpdateTrigger_t9A23744715BB464A18983EFDA8572E7E072F2168_CustomAttributesCacheGenerator,
	AsyncAnimatorIKTrigger_t24AD6CA9764F3989F15216EE3B65C5D1A3FB578B_CustomAttributesCacheGenerator,
	AsyncAnimatorMoveTrigger_t109E79622C585911298836CD8C085959205051F2_CustomAttributesCacheGenerator,
	AsyncApplicationFocusTrigger_t83032ABFF9213CDDE183A185023D79A09B30EF0D_CustomAttributesCacheGenerator,
	AsyncApplicationPauseTrigger_tFB7D6AF53DF627337CE24A5567B1878AC69EC8D0_CustomAttributesCacheGenerator,
	AsyncApplicationQuitTrigger_t8D8588A06A61153E49C94D0873DA7C845558AA53_CustomAttributesCacheGenerator,
	AsyncAudioFilterReadTrigger_t9A0B9AD7E4D6221B45BFAD2A835C3538AB6E3D17_CustomAttributesCacheGenerator,
	AsyncBecameInvisibleTrigger_t7B3C4DA88E3A4E5B1599B158394247EE9A1C7416_CustomAttributesCacheGenerator,
	AsyncBecameVisibleTrigger_t5CEB9F94A7DFEF8778D195406445FF0E6CF78DE8_CustomAttributesCacheGenerator,
	AsyncBeforeTransformParentChangedTrigger_t13CEAA0C6AFB2DCC5EF53AA599E09E81AA01BF14_CustomAttributesCacheGenerator,
	AsyncOnCanvasGroupChangedTrigger_t66DE0EE2E0D5450540DE5BFC91CA0B2FEC6314C1_CustomAttributesCacheGenerator,
	AsyncCollisionEnterTrigger_t254EEA82B58D99B798A0638BC10FE77309A7A256_CustomAttributesCacheGenerator,
	AsyncCollisionEnter2DTrigger_t887A6B3188DC7A14A7CA5F6CF01E4D2635C674B7_CustomAttributesCacheGenerator,
	AsyncCollisionExitTrigger_t0D0210C093D3725FE73ED6771AA1550423364524_CustomAttributesCacheGenerator,
	AsyncCollisionExit2DTrigger_t98CB78C1463FCD7F69FC1B84207FE0B864F0410A_CustomAttributesCacheGenerator,
	AsyncCollisionStayTrigger_tCCDE9A96609FB1A658F91D54A691CD6704CAA882_CustomAttributesCacheGenerator,
	AsyncCollisionStay2DTrigger_tDB4703E2F5EDA265DB5449D88FD5BC6AEB998BF2_CustomAttributesCacheGenerator,
	AsyncControllerColliderHitTrigger_t5134DD9C49CE4D2F2A67383C1A69DAFAA9D11A92_CustomAttributesCacheGenerator,
	AsyncDisableTrigger_t8CA9C5606CB0022AA26AB462199E651123653090_CustomAttributesCacheGenerator,
	AsyncDrawGizmosTrigger_t4958A3DF58C076C55DDD945EC07A00A6CC3498B3_CustomAttributesCacheGenerator,
	AsyncDrawGizmosSelectedTrigger_t57AB1A58E21CC60B54866CEB2A9FE5971E0A3074_CustomAttributesCacheGenerator,
	AsyncEnableTrigger_tB634428E53814A596931A6D574315FD716189C50_CustomAttributesCacheGenerator,
	AsyncGUITrigger_t36C147776091A27EF7527BC98560F8EBB9E808EA_CustomAttributesCacheGenerator,
	AsyncJointBreakTrigger_t874B2B61D4D48487690385F4E198E60C886A500B_CustomAttributesCacheGenerator,
	AsyncJointBreak2DTrigger_t3E666FA2DE906DE89CC79B692521AA6EE711934E_CustomAttributesCacheGenerator,
	AsyncParticleCollisionTrigger_t9D4FA39E8CADD41645732D3A820AE435219E2F5D_CustomAttributesCacheGenerator,
	AsyncParticleSystemStoppedTrigger_t0F0A22FA9EC6235AC4627C672F53BCA6D12467FC_CustomAttributesCacheGenerator,
	AsyncParticleTriggerTrigger_t2BCAAA60F894FB4AE1C5A26356B269D8587C226D_CustomAttributesCacheGenerator,
	AsyncParticleUpdateJobScheduledTrigger_tBA9445AF4934EF9F52D3AC6598CAF9B5DF514167_CustomAttributesCacheGenerator,
	AsyncPostRenderTrigger_tACEB9E79F5A6EE5510E8F74109640633BC6ECDB0_CustomAttributesCacheGenerator,
	AsyncPreCullTrigger_tDF118EEBDADECD321DDB7AD31B8120BC6818F9F7_CustomAttributesCacheGenerator,
	AsyncPreRenderTrigger_tD2CBFB8EAEEBCDB5838C71AC873689FFF961E285_CustomAttributesCacheGenerator,
	AsyncRectTransformDimensionsChangeTrigger_t0419A603C2E72B207F0506CBA2F2BC6FC03B2457_CustomAttributesCacheGenerator,
	AsyncRectTransformRemovedTrigger_tCDC782753E53846A9A9E308DDDD0E20EDC93C318_CustomAttributesCacheGenerator,
	AsyncRenderImageTrigger_t47A7E1D01FD9DB027C65E2064D2DD6253BC4AB0E_CustomAttributesCacheGenerator,
	AsyncRenderObjectTrigger_tB922BB87D329F98226329AA3E491F809B6379660_CustomAttributesCacheGenerator,
	AsyncServerInitializedTrigger_t087A376938D0580C2647B3B9B55E927E49C9FEBB_CustomAttributesCacheGenerator,
	AsyncTransformChildrenChangedTrigger_tCDE97CE1BF36149A3CB55E4A82D692758EDAD7D5_CustomAttributesCacheGenerator,
	AsyncTransformParentChangedTrigger_t0C0B10322954D9ED5C953A11A6786BBCB68F6396_CustomAttributesCacheGenerator,
	AsyncTriggerEnterTrigger_t2E9351C93BCA4677BA6D92D4C0CF8C22261BFC3B_CustomAttributesCacheGenerator,
	AsyncTriggerEnter2DTrigger_t999BD5719FBABEA2EDB0FEA5FDC8336A7DA76917_CustomAttributesCacheGenerator,
	AsyncTriggerExitTrigger_tD48E2EE94818B5C05D4C9E8106BD2597A101D3CC_CustomAttributesCacheGenerator,
	AsyncTriggerExit2DTrigger_t95DFB35379000F9CF3E4EA0601CD08127EC5462C_CustomAttributesCacheGenerator,
	AsyncTriggerStayTrigger_t48B35A72F3BA61F203488A57DB8F2A18200E12A5_CustomAttributesCacheGenerator,
	AsyncTriggerStay2DTrigger_tF772045DFB3DE353BA8175280CA52283D1EDFA11_CustomAttributesCacheGenerator,
	AsyncValidateTrigger_t97D6D0AF3C77858DDB0100B7E104A5555EF04C02_CustomAttributesCacheGenerator,
	AsyncWillRenderObjectTrigger_t24F3165363B8FF9783570A334C70F09BAE7A1BA4_CustomAttributesCacheGenerator,
	AsyncResetTrigger_tA217B969CD85B67D425B6B6F45E983643D73070F_CustomAttributesCacheGenerator,
	AsyncUpdateTrigger_t69C1AADCF9304CA32D768FB317C26365335C856E_CustomAttributesCacheGenerator,
	AsyncBeginDragTrigger_t0E46010A9E12309809FC2A823118141A9A7A1423_CustomAttributesCacheGenerator,
	AsyncCancelTrigger_tEF554944E4D80D51FA48B49D8A9182C75EC96D9D_CustomAttributesCacheGenerator,
	AsyncDeselectTrigger_tD36DCFE7A61E16AD00D20122783619413A5B19A0_CustomAttributesCacheGenerator,
	AsyncDragTrigger_t94BA4F7B71CB126E1C5B2936C576DC7EA831E7B4_CustomAttributesCacheGenerator,
	AsyncDropTrigger_t8C7A57AD20C3902E80FEF896EB61A045E30FEE8F_CustomAttributesCacheGenerator,
	AsyncEndDragTrigger_tDDB3674FA927983E51CA4CC1A03F23AB6A5D1846_CustomAttributesCacheGenerator,
	AsyncInitializePotentialDragTrigger_t219531E2B739E95B96CE192BA23FDFF80A62AC99_CustomAttributesCacheGenerator,
	AsyncMoveTrigger_t4FC53A1969EB5386E2D0B9B1088E858DD6896659_CustomAttributesCacheGenerator,
	AsyncPointerClickTrigger_t421FEC992B38EF3FC608AF062BF0D613EFB39D06_CustomAttributesCacheGenerator,
	AsyncPointerDownTrigger_t650AD4A1A7FD2A646205EC691F70079632D993F9_CustomAttributesCacheGenerator,
	AsyncPointerEnterTrigger_tE2D5FF7D6D00BE9AA4B2ED0C6817678D8D157747_CustomAttributesCacheGenerator,
	AsyncPointerExitTrigger_tA1C1FB5030F175CF21438BFF4DA3D549B31815B7_CustomAttributesCacheGenerator,
	AsyncPointerUpTrigger_tA88BA67B00881197DE089742729D35CDA1A2320C_CustomAttributesCacheGenerator,
	AsyncScrollTrigger_t575F13F3F86F4177127C331BE6E37B8A65F1DA21_CustomAttributesCacheGenerator,
	AsyncSelectTrigger_t2394AE85B5E1897148B2CAD3DB12DEF5A77CA9EC_CustomAttributesCacheGenerator,
	AsyncSubmitTrigger_t6BEA5852DA89E0A1358C4DCD4074E1B782969123_CustomAttributesCacheGenerator,
	AsyncUpdateSelectedTrigger_t0F1DF2441746047AA160610572BA5E06DBCB1CCF_CustomAttributesCacheGenerator,
	U3CU3Ec_t06DEAF86CBB5C462C627811F9852BDFF005A2073_CustomAttributesCacheGenerator,
	AsyncMethodBuilderAttribute_t396D945B42DB89B13143E1ED01C482827A3DEE30_CustomAttributesCacheGenerator_U3CBuilderTypeU3Ek__BackingField,
	PlayerLoopHelper_t2E5EE4D6CAA10845C9E0146D0A57E739FFA078DC_CustomAttributesCacheGenerator_U3CIsEditorApplicationQuittingU3Ek__BackingField,
	UniTaskScheduler_t9DBF8570AC748696C52D015E46344646DFA4B230_CustomAttributesCacheGenerator_UnobservedTaskException,
	AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_U3CCurrentU3Ek__BackingField,
	AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_U3CCysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_PrevU3Ek__BackingField,
	AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_U3CCysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_NextU3Ek__BackingField,
	AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_U3CCysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_PrevU3Ek__BackingField,
	AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_U3CCysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_NextU3Ek__BackingField,
	CancellationTokenExtensions_t23150039E4970811FFB82D4455E541A749E60158_CustomAttributesCacheGenerator_CancellationTokenExtensions_RegisterWithoutCaptureExecutionContext_mFA67F8204DAD04F3F4E4CC416B5E8055A343A08F,
	UniTaskStatusExtensions_t99C133AD36EB3F216D23147CD00206BD441F0E4C_CustomAttributesCacheGenerator_UniTaskStatusExtensions_IsCompleted_m7A9E3A6930C2EB3ED293F11782F2E0A16FAF8E71,
	PlayerLoopHelper_t2E5EE4D6CAA10845C9E0146D0A57E739FFA078DC_CustomAttributesCacheGenerator_PlayerLoopHelper_get_IsEditorApplicationQuitting_m41F5E63B94BAA7BC025829359D13C0101BD74551,
	PlayerLoopHelper_t2E5EE4D6CAA10845C9E0146D0A57E739FFA078DC_CustomAttributesCacheGenerator_PlayerLoopHelper_set_IsEditorApplicationQuitting_m8FE072C71C6415A874DC4E074DFA91B8E81C1630,
	PlayerLoopHelper_t2E5EE4D6CAA10845C9E0146D0A57E739FFA078DC_CustomAttributesCacheGenerator_PlayerLoopHelper_Init_mD46E29166AB848484B0BB715017D5CC9144E51E1,
	UniTask_tB2308A79346AE4017FEA07795B6E0A00C0D30B0F_CustomAttributesCacheGenerator_UniTask__ctor_mA1D2FAF1D02391065D770F56EE3CFD028157CACD,
	UniTask_tB2308A79346AE4017FEA07795B6E0A00C0D30B0F_CustomAttributesCacheGenerator_UniTask_get_Status_m5CCD7973CFD636CF45F67AF23CC34349ABBAD0AD,
	UniTask_tB2308A79346AE4017FEA07795B6E0A00C0D30B0F_CustomAttributesCacheGenerator_UniTask_GetAwaiter_m1FE35EBA18DB40141C1025CECD4FC300E3A8C59C,
	Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter__ctor_m5C4926AF1F0B75F44AC954B944D307066AA132CF,
	Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter_get_IsCompleted_mDCC71AAE7FA5EA2B303D3EC512177FD122C8DA49,
	Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter_GetResult_mFCA8BA81A9A7DA641DEFC7FC121FED87A3EC06CA,
	Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter_UnsafeOnCompleted_m1428D2FC0145C8CABB14E4B9A15F38B6AE34F8E5,
	Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter_SourceOnCompleted_m71BEAA335CDD0FBD24F14F9B970B8E1B69AE01B4,
	AwaiterActions_t98CE1739780D10DE070EA22469BDD661F7721BDD_CustomAttributesCacheGenerator_AwaiterActions_Continuation_m6C950CC84C028955999620A11573A87154D4F2B9,
	UniTask_1_t9FE8B3D4B9F6652AE80DA9C9E7F3F0DD133D1D94_CustomAttributesCacheGenerator_UniTask_1__ctor_mA66DD0CCDBB13655E9DABDFBA3CB51287728DCE3,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_Reset_m43C8782C2188C269E53275F197E11FAC7080AE17,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_TrySetResult_mED45D3F73A294D098843FD7DB5755A3BC313D9D2,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_TrySetCanceled_m46448E2FAA596ED6FC4842180BCE4BC8EBB934DF,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_GetStatus_mA6CA4535C4A74CED6D362E8E9C97C5D1B422349B,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_UnsafeGetStatus_m9801AAB80713DBD2CF427D616881D2C515670438,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_GetResult_mBA13129431126C629F6605F87934079053DB52C0,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_OnCompleted_m041EDDC3143C0479E4DBC228F1C0C131A253ECED,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_ValidateToken_m180C8A5395E191052AE7D41B5A8936104B4BE64E,
	UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_MarkHandled_m3EABFFA54BF7934490BCA86DA8F76AA023BE8144,
	UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_get_Task_m71FDFD642F5DE334B395253D95E812AF5E191CD1,
	UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_TrySetResult_mD0DF7B8E60DD4FE6D4FC5EE0386CE075805E12DF,
	UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_GetResult_m39A120F5B2805CF60DF0DDDF56AD13E6FE6BDED1,
	UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_GetStatus_mED83FEAAD02A8E3145F2DA6625390DDFFC374002,
	UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_UnsafeGetStatus_m4C223CE67FF35004CE732D6ED295B814E3FD9E02,
	UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_OnCompleted_mFF8DB2962201801A755B6B2EC59EFF6B3ADA4CC3,
	UniTaskCompletionSource_t91708088715CAB25AA896654AC4E5CEE41B31BCB_CustomAttributesCacheGenerator_UniTaskCompletionSource_TrySignalCompletion_m1DF031CC4D1BC7CC43F6CF57FB5056173615B064,
	UniTaskExtensions_t3DEAB33C9A9C207E6B284BB9BE4A70F923EC5F36_CustomAttributesCacheGenerator_UniTaskExtensions_Forget_m03A03134BA00CD83A83902AEF9352C8C0AC595A5,
	AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_set_Current_m09CA8F609DF7A066B02848DBD323C745B94508FF,
	AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_get_Prev_m921BEAE8DCA173B0DD2C65F5FEDDA6134711C459,
	AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_set_Prev_m9346EF053F0907FEBF8A15F16598192C0BD212C1,
	AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_get_Next_m608A053D4480BC7511E28B439894055610D88E51,
	AsyncTriggerEnumerator_t37A365907E9C30F3DF70F545FF4F614B3995A8EA_CustomAttributesCacheGenerator_AsyncTriggerEnumerator_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_set_Next_m57744F4DBDA686A7447D9BA2D71E18ECAB3A6FDB,
	AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_AsyncTriggerHandler_1_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_get_Prev_m76750E5C66C73E628A1321ABB67B4D38D33DFDAA,
	AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_AsyncTriggerHandler_1_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_set_Prev_mC5052FF72CAFC430F7C07D91BDCC571370D65EAE,
	AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_AsyncTriggerHandler_1_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_get_Next_m71AD68950B676B1AE5A547DF64D38ABA85D32EFC,
	AsyncTriggerHandler_1_t6F7CF7A6C6BB43006EFECE877464B4AB3127311A_CustomAttributesCacheGenerator_AsyncTriggerHandler_1_Cysharp_Threading_Tasks_ITriggerHandlerU3CTU3E_set_Next_m95AAB1B41971906EE6D9F1430D9E0889375A62CF,
	ContinuationQueue_t1AC570586E5949315476FE4A64A3D024C45886C2_CustomAttributesCacheGenerator_ContinuationQueue_RunCore_mA8B47350621EF411C87B9BF4255A6B608713CE65,
	PlayerLoopRunner_t3F49822FCB616AEED5CCB70D8AF755B67A5ECB9C_CustomAttributesCacheGenerator_PlayerLoopRunner_RunCore_m6C97C81CE05C65E05140C6F3FA67F4ACCB908BC1,
	Awaiter_t6DACE416FC47D39CDB55A3CE67458F1B9FC89FC0_CustomAttributesCacheGenerator_Awaiter__ctor_m5C4926AF1F0B75F44AC954B944D307066AA132CF____task0,
	UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A_CustomAttributesCacheGenerator_UniTaskCompletionSourceCore_1_t56A7A0990BC0ED3217E827DD4FDB186BBCF59C8A____Version_PropertyInfo,
	UniTask_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
